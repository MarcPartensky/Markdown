#!/usr/local/marc/markdown.py

# Todolist

## Obligatoire
- [ ] prêt d'honneur
- [ ] chercher des offres sur monster
- [ ] appeller la bpi
- [ ] carte de visite [affinity designer](/Volumes/$/Application/Affinity\ Designer.app) + [affinity photo](/Volumes/$/Application/Affinity\ Photo.app)

## Website
- [ ] envoie de mail
- [ ] chartre graphique page d'accueil
- [ ] ajouter connexion dans la page d'accueil
- [ ] ajouter formulaire qui fonctionne sur la page d'accueil
- [ ] section liste de projets sur site web dans tel ou tel techno = preuve pour freelancer.com
- [ ] 1 ou 2 page de cv
- [ ] fourier sur page d'accueil
- [ ] touch typing + django channels / websockets
- [ ] pdf viewer
- [ ] FTP 
- [ ] google login
- code app
    - [ ] live code
- todo app
    - [ ] vuejs todolist + synchronisation calendrier
    - [ ] fusion calendrier et todolist
- article app
    - [ ] article iframe
    - [ ] choix de thème par utilisateur button
    - [ ] choix de layout d'article par lecteur + layout par défaut pour un compte donné
    - [ ] édition de markdown en ligne + preview
    - [ ] meta données: public? + like button + auteur + compteur lectures + time
    - [ ] article download as pdf
    - [ ] go up button smoothly + header + footer
    - [ ] code exécutable, éditable, copiable, afficher langage


## Isep
### APP
- [ ] correction problème router
- [ ] check maxime header
- [ ] ajouter page login

## Promotion
- [ ] ajouter spark + aws emr au cv + linkedin + site web
- [ ] ajouter déploiement docker de fourier et pygame-geometry
- [ ] ajouter docker, tensorflow au cv
- [ ] bilan de compétences en latex
- [ ] version anglaise du CV

## Projets
- [ ] compétitions de programmation google kick start?, facebook?
- [ ] ctf?
- [ ] online liveshare codeshare.live
- [ ] créer un compte fiverr
- [ ] recoudre: pantalon + manteau

## Tutoriels
- [ ] 3d kit unity
- [ ] react native
- [ ] rust
- [ ] pytorch
- [ ] latex
- [ ] sklearn
- [ ] angular
- [ ] développement mobile java
- [ ] portail de l'informatique
- [ ] go dans go
- [ ] .Net
- [ ] docker + kubernetes
- [ ] redis
- [ ] scala

## Lectures
- [ ] zero to one
- [ ] bio Elon Musk
- [ ] "Quand la machine apprend" de Yann Le Cun
- [ ] manifeste agile
- [ ] 87/483 c# .NET
- [ ] préparation certification linux

## Bonus
- [ ] protection des secrets avec mac (ce dont parlait valentin)
- [ ] bot discord gestion de rôle automatique + leaderboard + rename avec €
- [ ] dotfiles
- [ ] améliorer photo sur cvs

## Tâche de fond
